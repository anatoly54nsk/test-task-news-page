#!/usr/bin/make
help:
	@echo "runDev		Запускает dev окружение"

SHELL = /bin/sh

MY_UID := $(shell id -u)
MY_GID := $(shell id -g)
MY_USER := ${USER}

export MY_UID
export MY_GID
export MY_USER

run:
	docker-compose -f environment/dev/docker-compose.yaml up
build:
	docker-compose -f environment/dev/docker-compose.yaml up --force-recreate --build
