import Vue from 'vue';
import Vuex from 'vuex';
import user from './user';
import adminCategory from './adminCategory';
import adminNews from './adminNews';
import news from './news';
import adminComment from './adminComment';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    adminCategory,
    adminNews,
    adminComment,
    news,
  },
});
