export default {
  namespaced: true,
  state: () => ({
    list: [],
    pagination: {},
  }),
  mutations: {
    setList(state, payload) {
      state.list = payload;
    },
    setPagination(state, payload) {
      state.pagination = payload;
    },
  },
  getters: {
    getList(state) {
      return state.list;
    },
    getPagination(state) {
      return state.pagination;
    },
  },
  actions: {
    async refresh({ commit }, { api, params }) {
      const response = await api.list(params);

      const { pagination, data } = response;

      commit('setList', data['hydra:member']);
      commit('setPagination', pagination);
    },
  },
};
