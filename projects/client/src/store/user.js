export default {
  namespaced: true,
  state: () => ({
    user: null,
  }),
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
  },
  actions: {
    async logIn({ commit }, { vm, params }) {
      try {
        const response = await vm.$api.auth.logIn(params);

        const user = response.data?.user ?? null;

        commit('setUser', user);
      } catch (e) {
        commit('setUser', null);
      }
    },
    logOut({ commit }) {
      commit('setUser', null);
    },
  },
};
