function initActivityRegisters(tree, list = {}) {
  tree.forEach((branch) => {
    list[branch.id] = false;
    if (branch.categories.length > 0) {
      list = initActivityRegisters(branch.categories, list);
    }
  });

  return list;
}

function getCategoryIdsRecursive(category, ids = []) {
  if (!category.id) {
    return [];
  }

  ids.push(category.id);

  if (category.categories.length > 0) {
    category.categories.forEach((nestedCategory) => {
      ids = getCategoryIdsRecursive(nestedCategory, ids);
    });
  }

  return ids;
}

function findCategoryByIdRecursive(categories, id) {
  return categories.reduce((found, category) => {
    if (+category.id === +id) {
      return category;
    }

    if (category.categories.length > 0) {
      return findCategoryByIdRecursive(category.categories, id);
    }

    return found;
  }, null);
}

export default {
  namespaced: true,
  state: () => ({
    tree: [],
    activityRegisters: {},
    activeCategory: {},
    news: [],
    newsPagination: {
      first: 1,
    },
    page: 1,
    perPage: 3,
    descSortOrder: true,
  }),
  mutations: {
    setTree(state, payload) {
      state.tree = payload;
    },
    setActivityRegisters(state, payload) {
      state.activityRegisters = payload;
    },
    setActiveCategory(state, payload) {
      state.activeCategory = payload;
    },
    setNews(state, payload) {
      state.news = payload;
    },
    setNewsPagination(state, payload) {
      state.newsPagination = payload;
    },
    setPage(state, payload) {
      state.page = payload;
    },
    setDescSortOrder(state, payload) {
      state.descSortOrder = payload;
    },
  },
  getters: {
    getTree(state) {
      return state.tree;
    },
    getActivityRegisters(state) {
      return state.activityRegisters;
    },
    getActiveCategory(state) {
      return state.activeCategory;
    },
    getNews(state) {
      return state.news;
    },
    getNewsPagination(state) {
      return state.newsPagination;
    },
    getPage(state) {
      return state.page;
    },
    getPerPage(state) {
      return state.perPage;
    },
    getDescSortOrder(state) {
      return state.descSortOrder;
    },
  },
  actions: {
    async refreshTree({ commit }, api) {
      const response = await api.getNotEmptyCategoriesTree();

      const { tree } = response.data;

      commit('setTree', tree);
      commit('setActivityRegisters', initActivityRegisters(tree));
    },
    changeActiveCategory({ commit, getters }, category) {
      const keys = Object.keys(getters.getActivityRegisters);
      const activityRegisters = keys.reduce((mapped, key) => {
        mapped[key] = +key === +category.id;
        return mapped;
      }, {});

      commit('setActivityRegisters', activityRegisters);
      commit('setActiveCategory', category);
    },
    changeActiveCategoryById({ dispatch, getters }, id) {
      const category = findCategoryByIdRecursive(getters.getTree, id);

      if (category) {
        dispatch('changeActiveCategory', category);
      }
    },
    changePage({ commit }, page) {
      commit('setPage', page);
    },
    changeDescSortOrder({ commit }, sortOrder) {
      commit('setDescSortOrder', sortOrder);
    },
    async refreshNewsList({ commit, getters }, api) {
      const params = {
        categoryIds: getCategoryIdsRecursive(getters.getActiveCategory),
        page: getters.getPage,
        perPage: getters.getPerPage,
        sortOrder: getters.getDescSortOrder ? 'desc' : 'asc',
      };

      const response = await api.getCategoryNews(params);

      const { news, pagination } = response.data;

      commit('setNews', news);
      commit('setNewsPagination', pagination);
    },
  },
};
