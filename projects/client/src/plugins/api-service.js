import AuthApiService from '../service/AuthApiService';
import RESTApiService, { RESTApiOptions } from '../service/RESTApiService';
import CustomApiService from '../service/CustomApiService';

const apiService = {
  install(Vue, options) {
    const { services, client } = options;

    Vue.prototype.$api = {};
    Vue.prototype.$api.auth = new AuthApiService(client);
    Vue.prototype.$api.rest = services.rest ? Object.keys(services.rest)
      .reduce((mapped, key) => {
        const url = services.rest[key];

        mapped[key] = new RESTApiService(url, client, RESTApiOptions);

        return mapped;
      }, {}) : {};

    Vue.prototype.$api.custom = new CustomApiService(client);
  },
};

export default apiService;
