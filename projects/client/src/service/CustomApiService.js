export default class CustomApiService {
  constructor(client) {
    this.client = client;
    this.baseUrl = '/api/custom';
  }

  getNotEmptyCategoriesTree() {
    return this.client.get(`${this.baseUrl}/getNotEmptyCategoriesTree`);
  }

  getCategoryNews(params) {
    return this.client.get(`${this.baseUrl}/getCategoryNews`, { params });
  }

  getArticleComments(params) {
    return this.client.get(`${this.baseUrl}/getArticleComments`, { params });
  }
}
