export default class AuthApiService {
  constructor(client) {
    this.client = client;
  }

  async logIn(user) {
    return this.client.post('/auth/login', user, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
