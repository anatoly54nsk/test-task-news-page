function getPagination(view = null) {
  if (!view) {
    return {
      first: null,
      last: null,
      next: null,
      previous: null,
      current: null,
    };
  }
  const re = /page=(\d+)/;
  let first = view['hydra:first']?.match(re) || [];
  first = first.length > 1 ? +first[1] : null;
  let last = view['hydra:last']?.match(re) || [];
  last = last.length > 1 ? +last[1] : null;
  let next = view['hydra:next']?.match(re) || [];
  next = next.length > 1 ? +next[1] : null;
  let previous = view['hydra:previous']?.match(re) || [];
  previous = previous.length > 1 ? +previous[1] : null;
  let current = view['@id'].match(re) || [];
  current = current.length > 1 ? +current[1] : null;

  return {
    first,
    last,
    next,
    previous,
    current,
  };
}

function transformResponse(response) {
  const { data } = response;
  const pagination = data ? getPagination(data['hydra:view']) : getPagination();
  return { data, pagination, total: data ? data['hydra:totalItems'] : 0 };
}

export default class RESTApiService {
  constructor(url, client, options) {
    this.url = url;
    this.client = client;
    this.options = options;
  }

  async create(entity) {
    return this.client.post(`${this.url}`, entity, { ...this.options.post }).then(transformResponse);
  }

  async get(id) {
    return this.client.get(`${this.url}/${id}`, { ...this.options.get }).then(transformResponse);
  }

  async list(params) {
    return this.client.get(this.url, { ...this.options.get, params }).then(transformResponse);
  }

  async update({ id, entity }) {
    return this.client.patch(`${this.url}/${id}`, entity, { ...this.options.patch }).then(transformResponse);
  }

  async replace({ id, entity }) {
    return this.client.put(`${this.url}/${id}`, entity, { ...this.options.put }).then(transformResponse);
  }

  async delete(id) {
    return this.client.delete(`${this.url}/${id}`, { ...this.options.delete }).then(transformResponse);
  }
}

const MIME_TYPE = 'application/ld+json';
const PATCH_MIME_TYPE = 'application/merge-patch+json';

export const RESTApiOptions = {
  patch: {
    headers: {
      Accept: MIME_TYPE,
      'Content-Type': PATCH_MIME_TYPE,
    },
  },
  put: {
    headers: {
      Accept: MIME_TYPE,
      'Content-Type': MIME_TYPE,
    },
  },
  post: {
    headers: {
      Accept: MIME_TYPE,
      'Content-Type': MIME_TYPE,
    },
  },
  get: {
    headers: {
      Accept: MIME_TYPE,
      'Content-Type': MIME_TYPE,
    },
  },
  delete: {
    headers: {
      Accept: MIME_TYPE,
      'Content-Type': MIME_TYPE,
    },
  },
};
