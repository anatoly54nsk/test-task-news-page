export default {
  rest: {
    category: '/api/categories',
    news: '/api/news',
    comments: '/api/comments',
  },
};
