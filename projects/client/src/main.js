import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import getRouter from './router';
import store from './store';
import apiService from './plugins/api-service';
import services from './service';

Vue.use(apiService, {
  services,
  client: axios,
});

Vue.config.productionTip = false;

const router = getRouter(store);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
