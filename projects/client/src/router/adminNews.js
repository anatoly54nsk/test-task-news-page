export default {
  path: 'news',
  name: 'admin-news',
  component: () => import('../views/Admin/NewsAdminView.vue'),
  meta: {
    title: 'Новости',
    authorizationNeeded: true,
    listDescription: [
      {
        fieldName: 'title',
      },
      {
        fieldName: 'dateCr',
        mapper: (value) => {
          const date = new Date(value).toLocaleDateString();
          return ` ${date}`;
        },
      },
    ],
    formConfig: [
      {
        fieldName: 'id',
        type: 'hidden',
      },
      {
        fieldName: 'title',
        fieldTitle: 'Заголовок',
        required: true,
        type: 'input',
      },
      {
        fieldName: 'description',
        fieldTitle: 'Описание',
        required: true,
        type: 'textarea',
      },
      {
        fieldName: 'isActive',
        fieldTitle: 'Опубликовать',
        required: true,
        type: 'checkbox',
      },
      {
        fieldName: 'text',
        fieldTitle: 'Полный текст',
        required: true,
        type: 'textarea',

      },
      {
        fieldName: 'category',
        fieldTitle: 'Категория',
        required: true,
        type: 'select',
        selectList: {
          api: 'category',
          itemTitle: 'title',
        },
        apiPath: '/api/categories',
      },
      {
        fieldName: 'dateCr',
        fieldTitle: 'Дата создания',
        type: 'timestamp',
      },
      {
        fieldName: 'dateUp',
        fieldTitle: 'Дата редактирования',
        type: 'timestamp',
      },
    ],
    layoutConfig: {
      addable: true,
      editable: true,
      deletable: true,
    },
  },
};
