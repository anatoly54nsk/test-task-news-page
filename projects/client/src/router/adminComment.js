export default {
  path: 'comments',
  name: 'admin-comments',
  component: () => import('../views/Admin/CommentAdminView.vue'),
  meta: {
    title: 'Комментарии',
    authorizationNeeded: true,
    listDescription: [
      {
        fieldName: 'author',
      },
      {
        fieldName: 'dateCr',
        mapper: (value) => {
          const date = new Date(value).toLocaleDateString();
          return ` ${date}`;
        },
      },
    ],
    formConfig: [
      {
        fieldName: 'id',
        type: 'hidden',
      },
      {
        fieldName: 'author',
        fieldTitle: 'Автор',
        required: true,
        type: 'input',
        disabled: true,
      },
      {
        fieldName: 'message',
        fieldTitle: 'Сообщение',
        required: true,
        type: 'textarea',
        disabled: true,
      },
      {
        fieldName: 'isActive',
        fieldTitle: 'Опубликовать',
        required: true,
        type: 'checkbox',
      },
    ],
    layoutConfig: {
      addable: false,
      editable: true,
      deletable: false,
    },
  },
};
