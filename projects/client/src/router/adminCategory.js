export default {
  path: 'category',
  name: 'admin-category',
  component: () => import('../views/Admin/CategoryAdminView.vue'),
  meta: {
    title: 'Категории',
    authorizationNeeded: true,
    listDescription: [
      {
        fieldName: 'title',
      },
    ],
    formConfig: [
      {
        fieldName: 'id',
        type: 'hidden',
      },
      {
        fieldName: 'title',
        fieldTitle: 'Заголовок',
        required: true,
        type: 'input',
      },
      {
        fieldName: 'parent',
        fieldTitle: 'Родительская категория',
        required: false,
        type: 'select',
        selectList: {
          api: 'category',
          itemTitle: 'title',
          additionalItem: {
            value: null,
            title: 'Без родительской категории',
          },
        },
        apiPath: '/api/categories',
      },
    ],
    layoutConfig: {
      addable: true,
      editable: true,
      deletable: true,
    },
  },
};
