import Vue from 'vue';
import VueRouter from 'vue-router';
import adminCategory from './adminCategory';
import adminNews from './adminNews';
import adminComment from './adminComment';

Vue.use(VueRouter);

const baseRoutes = [
  adminCategory,
  adminNews,
  adminComment,
].reduce((mapped, route) => {
  const routeWithPageNumber = { ...route };
  routeWithPageNumber.name = `${routeWithPageNumber.name}WithId`;
  routeWithPageNumber.path = `${routeWithPageNumber.path}/:id`;

  mapped.push(routeWithPageNumber);
  mapped.push(route);

  return mapped;
}, []);

// eslint-disable-next-line no-unused-vars
export default function getRouter(store) {
  const routes = [
    {
      path: '/admin',
      name: 'admin',
      component: () => import('../views/Admin/AdminView.vue'),
      meta: {
        title: 'Панель Администратора',
        authorizationNeeded: true,
      },
      children: [
        ...baseRoutes,
      ],
    },
    {
      path: '/',
      component: () => import('../views/ContentView.vue'),
      meta: {
        title: 'Главная',
        authorizationNeeded: false,
      },
      children: [
        {
          path: 'logIn',
          name: 'logIn',
          component: () => import('../views/LogInView.vue'),
          meta: {
            title: 'Логин',
            authorizationNeeded: false,
          },
        },
        {
          path: 'news/article/:id',
          name: 'newsArticle',
          component: () => import('../views/News/NewsArticleView.vue'),
          meta: {
            title: 'Новости',
            authorizationNeeded: false,
          },
        },
        {
          path: 'news/:id/:page',
          name: 'newsWithIdPagination',
          component: () => import('../views/News/NewsListView.vue'),
          meta: {
            title: 'Новости',
            authorizationNeeded: false,
          },
        },
        {
          path: 'news/:id',
          name: 'newsWithId',
          component: () => import('../views/News/NewsListView.vue'),
          meta: {
            title: 'Новости',
            authorizationNeeded: false,
          },
        },
        {
          path: '',
          name: 'home',
          component: () => import('../views/News/NewsListView.vue'),
          meta: {
            title: 'Новости',
            authorizationNeeded: false,
          },
        },
      ],
    },
  ];

  const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
  });

  router.beforeEach((to, from, next) => {
    const { getters } = store;

    if (to.meta?.authorizationNeeded && !getters['user/getUser']) {
      next('/login');
    }

    next();
  });

  return router;
}
