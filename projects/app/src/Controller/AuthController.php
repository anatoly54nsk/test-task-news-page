<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class AuthController extends AbstractController
{
    #[Route('/auth/login', name: 'auth_login')]
    public function index(#[CurrentUser] ?User $user): Response
    {
        return $this->json([
            'user' => [
                'name' => $user->getName(),
                'email' => $user->getEmail(),
            ],
        ]);
    }
}
