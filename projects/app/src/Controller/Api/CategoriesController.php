<?php

namespace App\Controller\Api;

use App\Entity\Category;
use App\Entity\News;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends AbstractController
{
    #[Route('/api/custom/getNotEmptyCategoriesTree', name: 'get_not_empty_categories_tree')]
    public function getNotEmptyCategoriesTree(EntityManagerInterface $entityManager)
    {
        $newsRepo = $entityManager->getRepository(News::class);
        $news = $newsRepo->findBy(['isActive' => true]);

        $parentCategories = array_values(array_unique(array_map(
            fn($newsItem) => $this->findParentCategoryRecursive($newsItem->getCategory()),
            $news,
        ), SORT_REGULAR));

        return $this->json(['tree' => $this->buildTreeRecursive($parentCategories)]);
    }

    #[Route('/api/custom/getCategoryNews', name: 'get_category_news')]
    public function getCategoryNews(Request $request, EntityManagerInterface $entityManager)
    {
        $categoryIds = $request->get('categoryIds', []);
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 3);
        $sortOrder = $request->get('sortOrder', 'desc');

        $repo = $entityManager->getRepository(News::class);
        $news = $repo->getFilteredByCategory(
            $categoryIds,
            $page,
            $perPage,
            $sortOrder,
        );
        $total = $repo->getFilteredByCategoryCount($categoryIds, $sortOrder);

        $pagination = $this->getPagination($page, $perPage, $total);

        return $this->json([
            'news' => $news,
            'pagination' => $pagination,
        ]);
    }

    #[Route('/api/custom/getArticleComments', name: 'get_article_comments')]
    public function getArticleComments(Request $request, EntityManagerInterface $entityManager)
    {
        $id = $request->get('id');

        $repo = $entityManager->getRepository(News::class);
        $news = $repo->find($id);

        $comments = array_values($news->getComments()->filter(fn($comment) => $comment->getIsActive())->toArray());

        return $this->json([
            'comments' => $comments,
        ]);
    }

    private function findParentCategoryRecursive(Category $category): Category
    {
        $parent = $category->getParent();
        if (!isset($parent)) {
            return $category;
        }

        return $this->findParentCategoryRecursive($parent);
    }

    private function buildTreeRecursive(array $categories): array
    {
        $tree = [];

        foreach ($categories as $category) {
            $branch = [
                'id' => $category->getId(),
                'name' => $category->getTitle(),
                'newsCount' => count($this->filterNews($category->getNews()->toArray())),
                'categories' => [],
            ];

            if ($category->getCategories()->count() > 0) {
                $branch['categories'] =  $this->buildTreeRecursive($category->getCategories()->toArray());
                $branch['newsCount'] = array_reduce($branch['categories'], function ($count, $category) {
                    $count += $category['newsCount'];
                    return $count;
                }, $branch['newsCount']);
            } else if ($this->isEmptyNews($category->getNews()->toArray())) {
                continue;
            }

            $tree[] = $branch;
        }

        return $tree;
    }

    private function isEmptyNews(array $news): bool
    {
        $filtered = $this->filterNews($news);

        return empty($filtered);
    }

    private function filterNews(array $news): array
    {
        return array_filter($news, fn($newsItem) => $newsItem->getIsActive());
    }

    public function getPagination(int $page, int $perPage, int $total): array
    {
        $last = (int) ceil($total / $perPage);

        return [
            'first'    => 1,
            'previous' => max($page - 1, 1),
            'current'  => $page,
            'next'     => min($page + 1, $last),
            'last'     => $last,
        ];
    }
}
