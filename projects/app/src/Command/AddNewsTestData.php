<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\News;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddNewsTestData extends Command
{
    protected static $defaultName = 'app:add-news-test-data';
    private EntityManagerInterface $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        string $name = null,
    )
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this->setHelp('Creates test data for categories and news...');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $json = '[{"title":"Спорт","categories":[{"title":"Обзор спортивных новостей района","categories":[],"news":[{"title":"Пловцы 52 школы заняли призовые места","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":false,"dateCr":"08.02.2022 14:00","dateUp":"08.02.2022 17:00"},{"title":"Большой тенис в городе Н","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"18.02.2022 11:00","dateUp":"19.02.2022 15:00"},{"title":"Трус не играет в хоккей!","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":false,"dateCr":"18.02.2022 11:00","dateUp":"19.02.2022 15:00"}]}],"news":[{"title":"Открылся новый футбольный клуб","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"01.01.2022 19:00","dateUp":"02.01.2022 10:00"},{"title":"Соревнования по боксу прошли","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"01.03.2022 12:00","dateUp":"05.03.2022 18:00"}]},{"title":"Астрологический вестник","categories":[{"title":"Что ждать згнакам огня в год тигра","categories":[],"news":[{"title":"Удача на строне овна","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"06.03.2022 15:00","dateUp":"10.03.2022 12:00"},{"title":"Стрельцам откроются большие возможности","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"16.03.2022 10:00","dateUp":"20.03.2022 11:00"},{"title":"Лев - царь зверей","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"15.03.2022 09:00","dateUp":"15.03.2022 11:00"}]},{"title":"Водные знаки зодиака в год тигра","categories":[],"products":[{"title":"Скорпионов ждет головокружительный успех","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"16.03.2022 10:00","dateUp":"17.03.2022 10:00"},{"title":"Раки и рыбы верные друзья в 2022м","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"16.03.2022 15:00","dateUp":"17.03.2022 19:00"}]}],"products":[{"title":"Астрологический прогноз от Павла Глобы","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"31.12.2021 10:00","dateUp":"01.02.2022 12:00"},{"title":"Союз тельца и девы","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"30.01.2021 14:00","dateUp":"02.02.2022 18:00"},{"title":"Эра водолея готовит сюрпризы","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text":"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ","isActive":true,"dateCr":"30.12.2021 15:00","dateUp":"31.12.2021 12:00"}]}]';
        $data = json_decode($json, true);
        foreach ($data as $datum) {
            $this->createCategory($datum, $this->entityManager);
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }

    private function createCategory(array $data, EntityManagerInterface $entityManager, ?Category $parentCategory = null)
    {
        $category = new Category();
        $category->setTitle($data['title']);
        if (isset($parentCategory)) {
            $category->setParent($parentCategory);
        }
        $entityManager->persist($category);

        if (!empty($data['news'])) {
            foreach ($data['news'] as $news) {
                $this->createNews($news, $entityManager, $category);
            }
        }

        if (!empty($data['categories'])) {
            foreach ($data['categories'] as $nestedCategory) {
                $this->createCategory($nestedCategory, $entityManager, $category);
            }
        }
    }

    private function createNews(array $newsData, EntityManagerInterface $entityManager, Category $category)
    {
        $dateCr = DateTimeImmutable::createFromFormat('d.m.Y H:i', $newsData['dateCr']);
        $dateUp = DateTimeImmutable::createFromFormat('d.m.Y H:i', $newsData['dateUp']);

        $news = new News();
        $news->setTitle($newsData['title']);
        $news->setDescription($newsData['description']);
        $news->setText($newsData['text']);
        $news->setIsActive($newsData['isActive']);
        $news->setDateCr($dateCr);
        $news->setDateUp($dateUp);
        $news->setCategory($category);

        $entityManager->persist($news);
    }
}
