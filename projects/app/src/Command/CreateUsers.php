<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUsers extends Command
{
    protected static $defaultName = 'app:create-users';
    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $hasher;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $hasher,
        string $name = null,
    )
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->hasher = $hasher;
    }

    protected function configure(): void
    {
        $this->setHelp('Creates test users...');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $credentials = [
            [
                'name' => 'root',
                'email' => 'root@root.ru',
                'password' => 'QWE123rty',
            ],
            [
                'name' => 'admin1',
                'email' => 'admin1@admin1.ru',
                'password' => 'QWE123rty',
            ],
            [
                'name' => 'admin2',
                'email' => 'admin2@admin2.ru',
                'password' => 'QWE123rty',
            ],
        ];

        foreach ($credentials as $credential) {
            $repo = $this->entityManager->getRepository(User::class);
            $user = $repo->findOneBy(['email' => $credential['email']]);

            if (isset($user)) {
                $output->writeln(["User {$user->getEmail()} exist"]);

                continue;
            }

            $user = new User();
            $user->setName($credential['name']);
            $user->setEmail($credential['email']);
            $password = $this->hasher->hashPassword($user, $credential['password']);
            $user->setPassword($password);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $output->writeln(["User {$user->getEmail()} added."]);
        }

        return Command::SUCCESS;
    }
}
