<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(News $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(News $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getFilteredByCategory(
        array $categoryIds,
        ?int $page,
        ?int $perPage,
        string $sortOrder,
    )
    {
        $query = $this
            ->getFilteredByCategoryQuery($categoryIds, $sortOrder)
            ->setMaxResults($perPage)
            ->setFirstResult($perPage * ($page - 1));

        return $query->getResult();
    }

    public function getFilteredByCategoryCount(
        array $categoryIds,
        string $sortOrder,
    )
    {
        $query = $this
            ->getFilteredByCategoryQuery($categoryIds, $sortOrder);

        return count($query->getResult());
    }

    public function getFilteredByCategoryQuery(array $categoryIds, string $sortOrder): Query
    {
        $qb = $this->createQueryBuilder('n');

        if (!empty($categoryIds)) {
            $qb
                ->where("n.category in (:ids)")
                ->setParameters(['ids' => $categoryIds]);
        }

        $qb
            ->andWhere("n.isActive = 1")
            ->orderBy('n.dateCr', $sortOrder);

        return $qb->getQuery();
    }
}
