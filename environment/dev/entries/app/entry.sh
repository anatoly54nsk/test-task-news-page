#!/bin/bash

if [[ "$1" = 'default' ]]; then
    chown -R $MY_UID:$MY_GID /var/www/app

    cd /var/www/app

    composer install

    php bin/console app:create-users
    php bin/console app:add-news-test-data

    until nc -z -v -w30 dev_mysql 3306
    do
        echo "Waiting for database connection..."
        # wait for 5 seconds before check again
        sleep 5
    done
fi

php-fpm -F -R
