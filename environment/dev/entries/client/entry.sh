#!/bin/sh

if [[ "$1" = 'default' ]]; then
  if [[ ! -f /var/www/client/src/main.js ]]; then
    node /develop/server/index.js
  else
    cd /var/www/client
    yarn
    yarn serve
  fi
fi
