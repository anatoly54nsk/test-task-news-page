#!/bin/bash

if [[ "$1" = 'default' ]]; then
    envsubst '$$MY_USER' < /deploy/nginx/nginx.conf > /etc/nginx/nginx.conf

    yes | cp -r /deploy/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
    yes | cp -r /deploy/nginx/conf.d/news-page.conf /etc/nginx/conf.d/news-page.conf
fi

nginx -g 'daemon off;'
